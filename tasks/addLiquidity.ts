import { task } from "hardhat/config";

task("addliq", "Add liquidity")
    .addParam("sender", "Address of sender")
    .addParam("token1", "Address of token1")
    .addParam("token2", "Address of token2")
    .addParam("token1desired", "Desired amount of token1")
    .addParam("token2desired", "Desired amount of token2")
    .addParam("token1min", "Minimum token1 amount")
    .addParam("token2min", "Minimum token2 amount")
    .addParam("to", "Recipient of the liquidity tokens")
    .addParam("adapter", "UniswapV2Router02 address")
    .setAction(async (taskArgs, hre) => {
        const adapter = await hre.ethers.getContractAt("UniswapAdapter", taskArgs.adapter);
        const sender = await hre.ethers.getSigner(taskArgs.sender);

        const tkn1 = await hre.ethers.getContractAt("Token", taskArgs.token1);
        const tkn2 = await hre.ethers.getContractAt("Token", taskArgs.token2);

        await tkn1.mint(
            taskArgs.sender,
            taskArgs.token1desired
        );
        await tkn2.mint(
            taskArgs.sender,
            taskArgs.token2desired
        )

        console.log(
            await tkn1.balanceOf(taskArgs.sender),
            await tkn2.balanceOf(taskArgs.sender),
        );

        await tkn1.connect(sender).approve(
            taskArgs.adapter,
            taskArgs.token1desired
        );
        await tkn2.connect(sender).approve(
            taskArgs.adapter,
            taskArgs.token2desired
        );
        
        await adapter.connect(sender).addLiquidity(
            taskArgs.token1,
            taskArgs.token2,
            taskArgs.token1desired,
            taskArgs.token2desired,
            taskArgs.token1min,
            taskArgs.token2min,
            taskArgs.to
        );
    });