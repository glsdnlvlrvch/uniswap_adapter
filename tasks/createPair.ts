import { task } from "hardhat/config";

task("createpair", "Add liquidity")
    .addParam("sender", "Address of sender")
    .addParam("token1", "Address of token1")
    .addParam("token2", "Address of token2")
    .addParam("adapter", "UniswapV2Factory address")
    .setAction(async (taskArgs, hre) => {
        const adapter = await hre.ethers.getContractAt("UniswapAdapter", taskArgs.adapter);
        const sender = await hre.ethers.getSigner(taskArgs.sender);
        
        const addr = await adapter.connect(sender).createPair(
            taskArgs.token1,
            taskArgs.token2,
        );

        console.log("Pair address - ", addr);
    });