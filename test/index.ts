import { expect } from "chai";
import { ethers } from "hardhat";
import { Signer, Contract, BigNumber } from "ethers";

describe("Adapter", function () {
    let TST: Contract;
    let ACDM: Contract;
    let POP: Contract;
    let Adapter: Contract;

    let signers: Signer[];

    beforeEach(async () => {
        const TSTFactory = await ethers.getContractFactory("Token");
        TST = await TSTFactory.deploy(
            "Test token",
            "TST"
        )
        await TST.deployed()

        const ACDMFactory = await ethers.getContractFactory("Token");
        ACDM = await ACDMFactory.deploy(
            "ACDM token",
            "ACDM"
        );
        await ACDM.deployed();

        const POPFactory = await ethers.getContractFactory("Token");
        POP = await POPFactory.deploy(
            "POP token",
            "POP"
        );
        await POP.deployed();

        const AdapterFactory = await ethers.getContractFactory("UniswapAdapter");
        Adapter = await AdapterFactory.deploy(
            "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f",
            "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
        );
        await Adapter.deployed();

        signers = await ethers.getSigners();

        await TST.mint(
            await signers[0].getAddress(),
            100000
        );
        await TST.approve(
            Adapter.address,
            10000
        );

        await ACDM.mint(
            await signers[0].getAddress(),
            100000
        )
        await ACDM.approve(
            Adapter.address,
            20000
        );

        await POP.mint(
            await signers[0].getAddress(),
            100000
        );
        await POP.approve(
            Adapter.address,
            200000
        );
    });

    it("create pairs && add liquidity && remove liquidity", async () => {
        const TST_ACDM = await Adapter.callStatic.createPair(
            TST.address,
            ACDM.address
        );

        const ACDM_POP = await Adapter.callStatic.createPair(
            ACDM.address,
            POP.address
        );

        await Adapter.addLiquidity(
            TST.address,
            ACDM.address,
            10000,
            10000,
            1,
            1,
            await signers[0].getAddress()
        );
        
        // const TST_ACDM_LP = await ethers.getContractAt("IUniswapV2Pair", TST_ACDM);
        // let lp_amount = await TST_ACDM_LP.balanceOf(
        //     await signers[0].getAddress()
        // );
        // await TST_ACDM_LP.transfer(
        //     Adapter.address,
        //     lp_amount
        // );

        // await Adapter.removeLiquidity(
        //     TST.address,
        //     ACDM.address,
        //     1,
        //     1,
        //     await signers[0].getAddress(),
        //     TST_ACDM
        // );

        await Adapter.addLiquidity(
            ACDM.address,
            POP.address,
            10000,
            10000,
            1,
            1,
            await signers[0].getAddress()
        );

        // const ACDM_POP_LP = await ethers.getContractAt("IUniswapV2Pair", ACDM_POP);
        // lp_amount = await ACDM_POP_LP.balanceOf(
        //     await signers[0].getAddress()
        // );
        // await ACDM_POP_LP.transfer(
        //     Adapter.address,
        //     lp_amount
        // );

        // await Adapter.removeLiquidity(
        //     ACDM.address,
        //     POP.address,
        //     1,
        //     1,
        //     await signers[0].getAddress(),
        //     ACDM_POP
        // );

        await Adapter.addLiquidityETH(
            POP.address,
            10000,
            1,
            ethers.utils.parseEther("0.000001"),
            await signers[0].getAddress(),
            {
                value: ethers.utils.parseEther("0.00001")
            }
        );
    });

    it("")
});
