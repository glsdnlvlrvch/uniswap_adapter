import { ethers } from "hardhat";

async function main() {
    const TSTFactory = await ethers.getContractFactory("Token");
    const TST = await TSTFactory.deploy(
        "Test token",
        "TST"
    )
    await TST.deployed()

    const ACDMFactory = await ethers.getContractFactory("Token");
    const ACDM = await ACDMFactory.deploy(
        "ACDM token",
        "ACDM"
    );
    await ACDM.deployed();

    const POPFactory = await ethers.getContractFactory("Token");
    const POP = await POPFactory.deploy(
        "POP token",
        "POP"
    );
    await POP.deployed();
    
    const Adapter = await ethers.getContractFactory("UniswapAdapter");
    const adapter = await Adapter.deploy(
        "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f",
        "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
    );

    await adapter.deployed();

    console.log("TST deployed to:", TST.address);
    console.log("ACDM deployed to:", ACDM.address);
    console.log("POP deployed to:", POP.address);
    console.log("Adapter deployed to:", adapter.address);
}

main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
