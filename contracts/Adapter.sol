//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract UniswapAdapter {
    address private _uniswapFactory;
    address private _uniswapRouter;

    constructor(
        address _factory,
        address _router
    ) {
        _uniswapFactory = _factory;
        _uniswapRouter = _router;
    }

    function createPair(
        address tokenA,
        address tokenB
    ) external returns(address) {
        return IUniswapV2Factory(_uniswapFactory).createPair(
            tokenA,
            tokenB
        );
    }

    function addLiquidity(
        address tokenA,
        address tokenB,
        uint amountADesired,  
        uint amountBDesired,  
        uint amountAMin,  
        uint amountBMin,  
        address to
    ) external {
        IERC20(tokenA).transferFrom(msg.sender, address(this), amountADesired);
        IERC20(tokenB).transferFrom(msg.sender, address(this), amountBDesired);

        IERC20(tokenA).approve(_uniswapRouter, amountADesired);
        IERC20(tokenB).approve(_uniswapRouter, amountBDesired);

        IUniswapV2Router02(_uniswapRouter).addLiquidity(
            tokenA,
            tokenB,
            amountADesired,  
            amountBDesired,  
            amountAMin,  
            amountBMin,  
            to,  
            block.timestamp
        );
    }

    function addLiquidityETH(
        address token,
        uint amountDesired,   
        uint amountMin,
        uint amountETHMin,   
        address to
    ) external payable {
        IERC20(token).transferFrom(msg.sender, address(this), amountDesired);
        IERC20(token).approve(_uniswapRouter, amountDesired);

        bytes memory callData = abi.encodeWithSignature(
            "addLiquidityETH(address,uint,uint,uint,address,uint)",
            token,
            amountDesired,
            amountMin,
            amountETHMin,
            to,
            block.timestamp
        );

        (bool success,) = _uniswapRouter.call{value: msg.value}(callData);
        require(
            success,
            "ERROR IN CALL"
        );
    }

    function removeLiquidity(
        address tokenA,  
        address tokenB,  
        // uint liquidity,  
        uint amountAMin,  
        uint amountBMin,  
        address to,
        address LPtoken
    ) external returns (
        uint amountA, 
        uint amountB
    ) {
        address pair = IUniswapV2Factory(_uniswapFactory).getPair(tokenA, tokenB);

        uint liquidity = IERC20(LPtoken).balanceOf(address(this));
        IERC20(pair).approve(_uniswapRouter, liquidity);

        return IUniswapV2Router02(_uniswapRouter).removeLiquidity(
            tokenA,
            tokenB,
            liquidity,
            amountAMin,
            amountBMin,
            to,
            block.timestamp
        );
    }

    function removeLiquidityETH(
        address token,
        uint liquidity,
        uint amountTokenMin,
        uint amountETHMin,
        address to,
        uint deadline
    ) external {}

    function getPrice(
        address tokenA, 
        address tokenB
    ) external returns (
        uint price,
        address token
    ) {
        address pair = IUniswapV2Factory(_uniswapFactory).getPair(tokenA, tokenB);
        (uint reserves0, uint reserves1,) = IUniswapV2Pair(pair).getReserves();
        uint price = reserves0 > reserves1 ? (reserves0 / reserves1) : (reserves1 / reserves0);
        address token = reserves0 > reserves1 ? tokenA : tokenB;

        return (price, token);
    }

    function swap(
        address tokenA,
        address tokenB,
        uint amount0Out, 
        uint amount1Out, 
        address to, 
        bytes calldata data
    ) external {
        address pair = IUniswapV2Factory(_uniswapFactory).getPair(tokenA, tokenB);
        IUniswapV2Pair(pair).swap(
            amount0Out,
            amount1Out,
            to,
            data
        );
    }
}
